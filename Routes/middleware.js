

/**
 *  ------------------------------------------------------------------------
 *  
 *  Database connection initialization
 *  
 * */

const DatabaseInfo = require('./config')
const MysqlInfo = require('./config').MysqlInfo;

const Sequelize = require('sequelize');
/*const db = new Sequelize(MysqlInfo.dbname, MysqlInfo.username, MysqlInfo.password, {
  host: 'localhost',
  // dialect: 'mysql'|'sqlite'|'postgres'|'mssql',
  dialect: 'mysql',
  operatorsAliases: false,

  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
  },

  // SQLite only
  // storage: 'path/to/database.sqlite'
});*/
// console.log(MysqlInfo);
const sequelize = new Sequelize('mysql://'+MysqlInfo.username+':'+MysqlInfo.password+'@localhost:3306/'+MysqlInfo.dbname);

sequelize.authenticate()
  .then(() => {
    console.log('Connection has been established successfully.');
  })
  .catch(err => {
    console.error('Unable to connect to the database:', err);
  });

exports.config={
    sequelize:sequelize
}
 