const env = require('node-env-file')

env(__dirname + '/.env')
module.exports={
    Port:process.env.PORT,
    MysqlInfo:{
    	dbname:process.env.MYSQLDATABASE,
    	username:process.env.MYSQLUSERNAME,
    	password:process.env.MYSQLPASSWORD
    },
}