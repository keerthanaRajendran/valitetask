const Sequelize = require('sequelize');
const sequelize = require('../Config/database').config.sequelize



const users = sequelize.define('users', {
    email_id: Sequelize.STRING,
    first_name: Sequelize.STRING,
    last_name: Sequelize.STRING,
    email_id: Sequelize.STRING,
    phone_number: Sequelize.STRING,
    role_id: Sequelize.BIGINT,
    password: Sequelize.STRING,
    reporting_user_id: Sequelize.BIGINT,
    created_by: Sequelize.STRING,
    updated_by: Sequelize.STRING,
    is_active: Sequelize.BIGINT,
    credential_notify_status: Sequelize.STRING,
})

const roles = sequelize.define('roles', {
    role_name: Sequelize.STRING,
    // role_name: { type: Sequelize.STRING,unique: true },
    role_code: Sequelize.STRING,
    reporting_type: Sequelize.STRING,
    reporting_role_id: Sequelize.INTEGER,
    user_count: Sequelize.STRING,
    is_active: Sequelize.BOOLEAN
})

const features = sequelize.define('features', {
    id: {
        type: Sequelize.BIGINT(20),
        primaryKey: true
    },
    feature_name: Sequelize.STRING,
    display_order: Sequelize.INTEGER
})

const subFeatures = sequelize.define('sub_features', {
    sub_feature_name: Sequelize.STRING
})

const features_relationship = sequelize.define('features', {
    id: {
        type: Sequelize.BIGINT(20),
        primaryKey: true
    },
    feature_name: Sequelize.STRING,
    display_order: Sequelize.INTEGER
})

const subFeatures_relationship = sequelize.define('sub_features', {
    sub_feature_name: Sequelize.STRING
})

const privileges = sequelize.define('role_feature_accesses', {
    feature_id: Sequelize.BIGINT(20),
    sub_feature_id: Sequelize.BIGINT(20),
    role_id: Sequelize.BIGINT(20)
})

const Op = Sequelize.Op;
module.exports = {
    users,
    roles,
    features,
    subFeatures,
    privileges

}
    